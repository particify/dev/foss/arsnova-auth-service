package de.thm.arsnova.service.authservice.model

data class RoomAccessEntry(
    val userId: String = "",
    val role: String = ""
)
