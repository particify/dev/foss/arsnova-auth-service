package de.thm.arsnova.service.authservice.model.event

data class RoomAccessSyncRequest(
    val roomId: String = ""
)
